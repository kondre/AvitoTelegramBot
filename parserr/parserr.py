import json
import requests
import time
from bs4 import BeautifulSoup
from requests import RequestException
import random

def get_html(url):
    print("get html by url: {}".format(url))

    USER_AGENTS = [
        'Mozilla/5.0 (Linux; Android 7.0; SM-G930VC Build/NRD90M; wv)',
        'Chrome/70.0.3538.77 Safari/537.36',
        'Opera/9.68 (X11; Linux i686; en-US) Presto/2.9.344 Version/11.00',
        'Mozilla/5.0 (compatible; MSIE 10.0; Windows 95; Trident/5.1)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_6) AppleWebKit/5342 (KHTML, like Gecko) Chrome/37.0.896.0 Mobile Safari/5342',
        'Mozilla/5.0 (Windows; U; Windows NT 6.2) AppleWebKit/533.49.2 (KHTML, like Gecko) Version/5.0 Safari/533.49.2',
        'Mozilla/5.0 (Windows NT 5.0; sl-SI; rv:1.9.2.20) Gecko/20110831 Firefox/37.0'
    ]
    headers = {
        'User-Agent': random.choice(USER_AGENTS)
    }

    proxies = {
         'http': 'http://proxy-brocker:8888',
         'https': 'http://proxy-brocker:8888'
    }

    proxy_http = requests.get(proxies['http'])
    proxy_https = requests.get(proxies['https'])

    if proxy_http.status_code != 200 and proxy_https.status_code != 200:
        raise RequestException

    response = requests.get(url, headers=headers, proxies=proxies)
    return response.content


def get_ads_list(avito_search_url):
    """
    :param avito_search_url: url like https://m.avito.ru/kazan/avtomobili/inomarki?pmax=200000&pmin=50000
    :return: ads list
    """
    html = get_html(avito_search_url)
    soup = BeautifulSoup(html, 'lxml')

    print("pagetitle:")
    print(soup.title.string)
    if soup.title.string in ["Доступ с вашего IP-адреса временно ограничен — Авито", "Доступ временно заблокирован"]:
        print("ip is blocked :-(")
        return []

    ads = soup.select('div[data-marker^="item-wrapper"]')

    print("========ads========")
    print(ads)

    ads_list = []
    for ad in ads:

            ad_url=ad.select_one('a[data-marker="item/link"]').get('href')
            ad_id=ad_url.split("_")[-1]

            ads_list.append({
                'id': ad_id,
                'url': ad_url,
#                'title': ad_header.replace(u'\xa0', u' '),
#                'price': ad_price.replace(u'\xa0', u' '),
#                'img': ad_img
            })

    return ads_list


def get_new_ads(new, old):
    _ = []
    for ad in new:
        if ad not in old:
            _.append(ad)
    return _
